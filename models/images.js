const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

const Schema = mongoose.Schema;

const imagesSchema = new Schema({
  url: {
    type: String,
    required: true,
  },
  likes: {
    type: Number,
    required: true,
  },
  userId: {
    type: Number,
    required: true
  },
}, { timestamps: true });

const ImagesTable = mongoose.model('images', imagesSchema);
module.exports = ImagesTable;