const express = require('express');
const path = require('path');
const serverless = require('serverless-http');
const fs = require('fs');

const fileUpload = require('express-fileupload');
const mongoose = require('mongoose');


const app = express();

const imageRoutes = require('./routes/imageRoutes')

app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));
app.use(fileUpload());


app.set('view engine', 'ejs');

app.use(express.static('public'));



//app.use(express.urlencoded({extended :true})); // pass all url encoded from form to an object
//app.use(express.json());

 var port = 8080;//process.env.PORT || 3000
// conect string to DB
const dbURI = "mongodb+srv://Aram_is_testing:aram_90_94@mongodb-free-cluster.vbqgj.mongodb.net/petYarn?retryWrites=true&w=majority";

var log = function(entry) {
    fs.appendFileSync('/tmp/petyarn.log', new Date().toISOString() + ' - ' + entry + '\n');
};

log(`------------------------------------------------------------------------`+"\n");


log(`port is ${port}`+"\n");

try {
    mongoose.connect(dbURI, { useNewUrlParser:true, useUnifiedTopology:true })
        .then( (result) => {
            // listen for resquest , after connecting to database
            app.listen(port);
            console.log("connected to DB")
            log("connected to DB");
            log(`listening to port ${port}`);
        })
        .catch( (err) => {console.log(err)})

} catch (error) {
    log(error.message);
 console.log("error on conecting to MogoDB",error);   
}


try{


app.get('/' , async (req, res) => {
    //res.render('index', { title: 'Home' });
    res.redirect(`/home`)
});

// app.get('/home' , async (req, res) => { 
//     res.render('index', { title: 'Home' });
// });

app.use(`/home`,imageRoutes);

app.get('/about' , async (req, res) => {
    res.render('about', { title: 'About Us' });
});
app.get('/login' , async (req, res) => {
    res.render('login', { title: 'Log In' });
});

app.get('/logout' , async (req, res) => {
    res.render('logout', { title: 'Log Out' });
});

app.get('/register' , async (req, res) => {
    res.render('register', { title: 'Register' });
});


app.use(function(req, res, next) {
    res.status(404);
    res.send('404: File Not Found');
});

}catch(error){
    log(error.message);
}
/* for local testing*/
//var process
//app.listen(3000, () => console.log(`Listening on: 3000`));


 //module.exports.handler = serverless(app);
