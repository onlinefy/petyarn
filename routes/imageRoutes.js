const express = require('express');
const imagesController = require('../controllers/imagesController');
const fs = require('fs');

const router = express.Router();

console.log("-------------------");

fs.appendFileSync('/tmp/petyarn.log', new Date().toISOString() + ' - ' + "in routing" + '\n');


router.get('/', imagesController.image_index);
router.post('/imageUpload', imagesController.image_add_new);
router.put('/likeImage/:id', imagesController.image_update);



router.delete('/:id', imagesController.image_delete);

module.exports = router;