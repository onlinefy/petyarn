const Image = require('../models/images');
var AWS = require('aws-sdk');
const fs = require('fs');

const image_index = (req, res) => {
  Image.find().sort({ createdAt: -1 })
    .then(result => {
      //console.log(result);
      res.render('home', { images: result, title: 'Home' });
    })
    .catch(err => {
      console.log(err);
    });
}


const image_add_new = async (req, res) => {

    AWS.config.update({
        accessKeyId: "AKIA364UP45SYJ7C6UPZ", // Access key ID
        secretAccesskey: "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", // Secret access key
        region: "ap-southeast-2" //Region
    })
    //console.log(req)
    try{
        const s3 = new AWS.S3();
        // Binary data base64
        const fileContent  = Buffer.from(req.files.fileInput.data, 'binary');
        //console.log("req.files",req.files);
        const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
        if (!validImageTypes.includes(req.files.fileInput.mimetype)) {
            throw "invaild image type"
        }
        let ext = req.files.fileInput.name.substr(req.files.fileInput.name.lastIndexOf('.') + 1);
        let splitFileName = req.files.fileInput.name.split(`.${ext}`);
        let fileName = splitFileName[0].replace(/[ `~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '_');
        fileName = fileName +'_'+ new Date().getTime() + "."+ext;


        const params = {
            Bucket: 'project-res',
            Key: `petyarn/assets/img/${fileName}`, // File name you want to save as in S3
            Body: fileContent,
            ACL:'public-read'
        };
        fs.appendFileSync('/tmp/petyarn.log', new Date().toISOString() + ' - ' + "in Create new" + '\n');
        //console.log("params",params)
        // Uploading files to the bucket
        await s3.upload(params, function(err, data) {
            if (err) {
                fs.appendFileSync('/tmp/petyarn.log', new Date().toISOString() + ' - ' + JSON.stringify(err) + '\n');
                res.redirect('/home');
            }else{
              let body = {
                url: data.Location,
                likes:0,
                userId:0
              }
            
              const image = new Image(body);
              image.save()
                .then(result => {
                  res.redirect('/home');
                })
                .catch(err => {
                  res.redirect('/home');
                  console.log(err);
                });
            }
        });

    }catch(error){
        console.log("--------------",error);
        fs.appendFileSync('/tmp/petyarn.log', new Date().toISOString() + ' - ' + error.message + '\n');
        throw "Internal Server Error";
    }
}



const image_update = async (req, res) => {
  const id = req.params.id;
  let data = await Image.findById(id);
  //console.log(data);


  Image.findByIdAndUpdate(
    { _id: id },
    { likes: data.likes+1 },
    function(err, result) {
      if (err) {
        res.send(err);
      } else {
        res.json({ redirect: '/home' });
      }
    }
  );
}




const image_delete = (req, res) => {
  const id = req.params.id;
  Image.findByIdAndDelete(id)
    .then(result => {
      res.json({ redirect: '/home' });
    })
    .catch(err => {
      console.log(err);
    });
}



module.exports = {
  image_index, 
  image_add_new, 
  image_delete,
  image_update
}